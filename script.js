{
    const { $, document, alert } = this;
    let bookEditNumber; // Номер редактируемой книги
    const div = document.getElementById('booksList');
    const divBooksForm = document.getElementById('bookForm');
    let formsTemplate;
    const books = [
        {
            cover: 'cover1.jpg',
            name: 'Алгебра и начала математического анализа',
            author: 'Колягин Юрий Михайлович, Шабунин Михаил Иванович, Федорова Надежда Евгеньевна , Ткачева Мария Владимировна ',
            year: '2015',
        },
        {
            cover: 'cover3.jpg',
            name: 'Геометрия',
            author: 'Атанасян Левон Сергеевич, Бутузов Валентин Федорович, Кадомцев Сергей Борисович , Позняк Эдуард Генрихович, Киселева Людмила Сергеевна',
            year: '2017',
        },
        {
            cover: 'cover2.jpg',
            name: 'Русский язык',
            author: 'Греков Василий Федорович, Чешко Лев Антонович, Крючков Сергей Ефимович',
            year: '2017',
        },
        {
            cover: 'cover4.jpg',
            name: 'Физика',
            author: 'А.В. Перышкин',
            year: '2008',
        },
    ];
    const formInput = [
        {
            label: 'Добавление',
            nameId: 'name2',
            authorId: 'author2',
            yearId: 'year2',
            coverId: 'cover2',
            saveBook: 'addBookSave()',
            cancel: 'hideAddForm()',
        },
        {
            label: 'Редактирование',
            nameId: 'name1',
            authorId: 'author1',
            yearId: 'year1',
            coverId: 'cover1',
            saveBook: 'editBookSave()',
            cancel: 'hideEditForm()',
        },
    ];
    function makeBookFormTemplate() {
        formsTemplate = this.makeTemplate('#forms-template');
    }
    $('#bookForm').hide();
    this.hideEditForm = function hideEditForm() {
        $('#bookForm').hide();
        $('#addBookButtonForm').show();
    };
    function showEditForm() {
        $('#bookForm').show();
        $('#addBookButtonForm').hide();
    }
    this.hideAddForm = function hideAddForm() {
        $('#bookForm').hide();
        document.getElementById('name2').value = '';
        document.getElementById('cover2').value = '';
        document.getElementById('year2').value = '';
        document.getElementById('author2').value = '';
        $('#addBookButtonForm').show();
    };
    this.showAddForm = function showAddForm() {
        $('#addBookButtonForm').hide();
        divBooksForm.innerHTML = '';
        $(divBooksForm).append(formsTemplate(formInput[0]));
        $('#bookForm').show();
    };
    // заполнение формы редактирования
    this.inputEditDetails = function inputEditDetails(book) {
        showEditForm();
        divBooksForm.innerHTML = '';
        $(divBooksForm).append(formsTemplate(formInput[1]));
        $('#author1').val(books[book.id].author);
        $('#name1').val(books[book.id].name);
        $('#year1').val(books[book.id].year);
        $('#cover1').val(books[book.id].cover);
        this.scrollTo(0, 0);
        bookEditNumber = book.id;
    };
    // удалить
    this.deleteBook = function deleteBook(book) {
        $('#bookForm').hide();
        books.splice(book.id, 1);
        drawBooksList();
    };
    // сохранить из формы редактирования
    this.editBookSave = function editBookSave() {
        if ($('#year1').val() <= 2017 && $('#year1').val() > -1 && $('#year1').val() !== '') {
            books[bookEditNumber].name = $('#name1').val();
            books[bookEditNumber].author = $('#author1').val();
            books[bookEditNumber].year = $('#year1').val();
            books[bookEditNumber].cover = $('#cover1').val();
            drawBooksList();
            $('#bookForm').hide();
        } else {
            alert('Неправильно введён год');
        }
    };
    // добавление
    this.addBookSave = function addBookSave() {
        if ($('#year2').val() <= 2017 && $('#year2').val() > -1 && $('#year2').val() !== '') {
            books.push({
                cover: $('#cover2').val(), name: $('#name2').val(), author: $('#author2').val(), year: $('#year2').val(),
            });
            drawBooksList();
            $('#bookForm').hide();
            document.getElementById('name2').value = '';
            document.getElementById('cover2').value = '';
            document.getElementById('year2').value = '';
            document.getElementById('author2').value = '';
        } else {
            alert('Неправильно введён год');
        }
    };
    this.makeTemplate = function makeTemplate(templateId) {
        const templateScript = $(templateId).html();
        return this.Handlebars.compile(templateScript);
    };
    $('#listTemplateScript').load('books-list-template.html', () => { drawBooksList(); });
    $('#formTemplateScript').load('book-form-template.html', () => { makeBookFormTemplate(); });
    function drawBooksList() {
        div.innerHTML = '';
        const template = this.makeTemplate('#books-template');
        $(div).append(template(books));
    }
}
